import React from 'react'
import { Toggle } from '@vast-ui/toggle'

const Accordian = () => (
  <Toggle>
    {(toggle, { isToggled }) => (
      <div className="Accordian">
        <button onClick={toggle}>Toggle</button>
        {isToggled && <div>Body</div>}
      </div>
    )}
  </Toggle>
)

export { Accordian }
