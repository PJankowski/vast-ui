import React from 'react'
import { Toggle } from '../src'

export let name = 'Basic'

export let Example = () => (
  <Toggle>
    {(toggle, { isToggled }) => (
      <div>
        <button onClick={toggle}>Toggle</button>
        {isToggled && <p>I can be seen</p>}
      </div>
    )}
  </Toggle>
)
