import { Component } from 'react'

class Toggle extends Component {
  constructor(props) {
    super(props)

    this.state = {
      toggled: false,
    }

    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState(state => ({
      toggled: !state.toggled,
    }))
  }

  render() {
    const { toggled } = this.state
    return this.props.children(this.toggle, {
      isToggled: toggled,
    })
  }
}

export { Toggle }
