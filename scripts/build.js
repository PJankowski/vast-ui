const execSync = require('child_process').execSync

const babel = `${__dirname}/../node_modules/.bin/babel`

const exec = (command, opts) =>
  execSync(command, {
    stdio: 'inherit',
    env: Object.assign({}, process.env, opts),
  })

exec(`${babel} src -d es --ignore *.test.js`, {
  BABEL_ENV: 'es',
})

exec(`${babel} src -d . --ignore *.test.js`, {
  BABEL_ENV: 'cjs',
})
